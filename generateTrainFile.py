import os
import random

currentPath = os.getcwd()
img_folder = os.path.join(currentPath, 'images/train')
img_names = os.listdir(img_folder)

train_file = open('images/train.txt', 'w')
val_file = open('images/val.txt', 'w')
for i, img_name in enumerate(img_names):
    img_path = os.path.join(img_folder, img_name)
    category = img_name.split('.')[0]
    if category == 'cat':
        label = 1
    else:
        label = 0
    content = img_path + ' ' + str(label) + '\n'
    rand = random.uniform(0, 1)
    if rand >= 0.7:
        val_file.write(content)
    else:
        train_file.write(content)

    print('{}/{}'.format(i, len(img_names)))

train_file.close()
val_file.close()
