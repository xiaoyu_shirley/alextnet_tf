"""
    This is the demo evaluating ImageNet weights: bvlc_alexnet.npy
"""

import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import cv2
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

from alexnet import AlexNet
from caffe_classes import class_names

DISPLAY = False

imagenet_mean = np.array([104., 117., 124.], dtype=np.float32)

current_dir = os.getcwd()

# put example images under images folder
image_dir = os.path.join(current_dir, 'images')
image_names = os.listdir(image_dir)

imgs = []
for image_name in image_names:
    imgs.append(cv2.imread(os.path.join(image_dir, image_name)))

if DISPLAY:
    fig = plt.figure(figsize=(15, 6))
    for i, img in enumerate(imgs):
        # 3 is the number of example images
        fig.add_subplot(1, 3, i + 1)
        plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
        plt.axis('off')

# placeholder for input and dropout rate
x = tf.placeholder(tf.float32, [1, 227, 227, 3])
keep_prob = tf.placeholder(tf.float32)

# create model without train_layer
model = AlexNet(x, keep_prob=keep_prob, num_classes=1000, skip_layer=[])
score = model.fc8

softmax = tf.nn.softmax(score)


# start a session to get the softmax result
def demo():
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        model.load_initial_weights(sess)
        fig2 = plt.figure(figsize=(15, 6))

        for i, image in enumerate(imgs):
            # Convert image to float32 and resize to (227x227)
            img = cv2.resize(image.astype(np.float32), (227, 227))
            img -= imagenet_mean
            img = img.reshape((1, 227, 227, 3))

            probs = sess.run(softmax, feed_dict={x: img, keep_prob: 1})

            # Get the class name of the class with the highest probability
            class_name = class_names[np.argmax(probs)]

            # Plot image with class name and prob in the title
            fig2.add_subplot(1, 3, i + 1)
            plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
            plt.title("Class: " + class_name + ", probability: %.4f" % probs[0, np.argmax(probs)])
            plt.axis('off')

        plt.show()


if __name__ == '__main__':
    demo()
