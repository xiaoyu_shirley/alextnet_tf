"""
    This is the demo using frzon_model.pb file
"""

import os
import numpy as np
import cv2
import time
from matplotlib import pyplot as plt
import tensorflow as tf

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

if tf.__version__ < '1.4.0':
    raise ImportError("Please upgrade your tensorflow installation to v1.4.0 at least!")

PATH_TO_MODEL = 'weights/finetune_alexnet/frozen_model.pb'

detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_MODEL, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

if __name__ == '__main__':
    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            image_tensor = detection_graph.get_tensor_by_name('image:0')
            keep_prob = detection_graph.get_tensor_by_name('keep_prob:0')
            prediction = detection_graph.get_tensor_by_name('predict:0')
            image_np = cv2.imread('images/dog.png')
            image_np = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
            image_np_expanded_single = np.expand_dims(image_np, axis=0)  # 1 x w x h x c

            """
                one problem is we can only use 128 images at the same time :(
            """
            image_np_expanded = image_np_expanded_single
            for i in range(128 - 1):
                image_np_expanded = np.vstack((image_np_expanded, image_np_expanded_single))

            start_time = time.time()
            [pred] = sess.run(
                [prediction],
                feed_dict={image_tensor: image_np_expanded,
                           keep_prob: 1})
            end_time = time.time()

            # For printing the predicted label
            output = tf.Print(pred, [pred], message='prediction: ')
            output.eval()
