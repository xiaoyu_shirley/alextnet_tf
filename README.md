### Fine tune AlexNet with custom dataset

- How to test
    0. Download imagenet-trained weights from [the link](http://www.cs.toronto.edu/~guerzhoy/tf_alexnet/bvlc_alexnet.npy)
    1. Put the weight file under `weights` folder
    2. Download several images for example (3 for default setting) and put udner `images`
    3. `python demo.py`