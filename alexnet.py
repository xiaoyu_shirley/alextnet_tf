from layers import *
import numpy as np


class AlexNet(object):
    def __init__(self, x, keep_prob, num_classes, skip_layer,
                 weights_path='DEFAULT'):
        """
        Inputs:
        :param x: tf.placeholder, for the input images
        :param keep_prob: tf.placeholder, for the dropout rate
        :param num_classes: int, number of classes
        :param skip_layer: list of strings, names of the layers you want to train from scratch
        :param weights_path: string, path to the pretrained weights (bvlc_alexnet.npy)
        """

        # Parse input arguments
        self.X = x
        self.NUM_CLASSES = num_classes
        self.KEEP_PROB = keep_prob
        self.SKIP_LAYER = skip_layer

        if weights_path == 'DEFAULT':
            # download link from http://www.cs.toronto.edu/~guerzhoy/tf_alexnet/bvlc_alexnet.npy
            # and put it under weights
            self.WEIGHTS_PATH = 'weights/bvlc_alexnet.npy'
        else:
            self.WEIGHTS_PATH = weights_path

        # build the graph of AlexNet
        self.create()

    def create(self):
        conv1 = conv(self.X, 11, 11, 96, 4, 4, padding='VALID', name='conv1')
        norm1 = lrn(conv1, 2, 1e-05, 0.75, name='norm1')
        pool1 = max_pool(norm1, 3, 3, 2, 2, padding='VALID', name='pool1')

        conv2 = conv(pool1, 5, 5, 256, 1, 1, groups=2, name='conv2')
        norm2 = lrn(conv2, 2, 1e-05, 0.75, name='norm2')
        pool2 = max_pool(norm2, 3, 3, 2, 2, padding='VALID', name='pool2')

        conv3 = conv(pool2, 3, 3, 384, 1, 1, name='conv3')

        conv4 = conv(conv3, 3, 3, 384, 1, 1, groups=2, name='conv4')

        conv5 = conv(conv4, 3, 3, 256, 1, 1, groups=2, name='conv5')
        pool5 = max_pool(conv5, 3, 3, 2, 2, padding='VALID', name='pool5')

        flattened = tf.reshape(pool5, [-1, 6 * 6 * 256])
        fc6 = fc(flattened, 6 * 6 * 256, 4096, name='fc6')
        dropout6 = dropout(fc6, self.KEEP_PROB)

        fc7 = fc(dropout6, 4096, 4096, name='fc7')
        dropout7 = dropout(fc7, self.KEEP_PROB)

        self.fc8 = fc(dropout7, 4096, self.NUM_CLASSES, dorelu=False, name='fc8')

        self.predict = tf.argmax(self.fc8, 1, name='predict')

    def load_initial_weights(self, session):
        weights_dict = np.load(self.WEIGHTS_PATH).item()
        for op_name in weights_dict:
            if op_name not in self.SKIP_LAYER:
                with tf.variable_scope(op_name, reuse=True):
                    for data in weights_dict[op_name]:
                        if len(data.shape) == 1:
                            var = tf.get_variable('biases', trainable=False)
                            session.run(var.assign(data))
                        else:
                            var = tf.get_variable('weights', trainable=False)
                            session.run(var.assign(data))
